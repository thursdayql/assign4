
//********************************************************************
//File:         PalindromeTester.java       
//Author:       Liam Quinn
//Date:         October 17th
//Course:       CPS100
//
//Problem Statement:
// Create a modified version of the PalindromeTester program so 
// that spaces, punctuation, and changes in upper-case and lower-case
// are not considered when determining whether a string is a
// palindrome.
// 
//Inputs: String that is a palindrome
//Outputs:  A statement telling reader whether the input was/was not a
//          palindrome.
// 
//********************************************************************

import java.util.*;


public class PalindromeTester
{

  public static void main(String[] args)
  {
    String str, another = "y";
    int left, right;

    PalindromeSolver input = new PalindromeSolver();
    
    Scanner scan = new Scanner(System.in);

    while (another.equalsIgnoreCase("y"))
    {
      System.out.println("Enter a potential palindrome: ");
      str = scan.nextLine();
      
      str = input.solvePalindrome(str);

      System.out.println("The program converted your input to: \"" + str + "\""
                         + " so it can be read by the program properly.");

      left = 0;
      right = str.length() - 1;

      while (str.charAt(left) == str.charAt(right) && left < right)
      {
        left++;
        right--;
      }

      System.out.println();

      if (left < right)
        System.out.println("That string is NOT a palindrome");
      else
        System.out.println("That string IS a palindrome");

      System.out.println();
      System.out.println("Test another palindrome (y/n)?");
      another = scan.nextLine();
    }
    scan.close();
  }

}
