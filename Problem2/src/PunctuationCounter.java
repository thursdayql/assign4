
//********************************************************************
//File:         PunctuationCounter.java       
//Author:       Liam Quinn
//Date:         October 17th
//Course:       CPS100
//
//Problem Statement:
// Design and implement a program the counts the number of
// punctuation marks in a text input file. Produce a table that
// shows how many times each symbol occurred.
// 
//Inputs: None
//Outputs:  The number of punctuation marks counted by the program
//          inside of the linked .txt file
// 
//********************************************************************

import java.util.*;
import java.io.*;


public class PunctuationCounter
{

  public static void main(String[] args) throws IOException
  {
   Scanner fileScan = new Scanner(new File("frankenstein.txt"));
    int lineIndex = 0;
    final int FINALLINEINDEX = 7224;
    String textStr = "";

    System.out.println("Processing...");
    System.out.println("");

    while (lineIndex != FINALLINEINDEX)
    {
      textStr = textStr + " " + fileScan.nextLine();
      lineIndex++;
    }

    // Counts the amount of characters in the file
    System.out.println("Ammount of characters in file before shearing: "
                       + textStr.length());

    //  Removes all characters that aren't punctuation marks
    textStr = textStr.toLowerCase();
    textStr = textStr.replaceAll("[a-z]", "");
    textStr = textStr.replaceAll(" ", "");
    textStr = textStr.replaceAll("[0-9]", "");
    System.out.println("Ammount of characters in file after shearing: "
                       + textStr.length());
    System.out.println("");
    
    System.out.println("File without alphabetical characters, numbers,"
                         + " or spaces: " + textStr);

    System.out.println("");

    // COUNTERS

    int counterPeriod = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '.')
      {
        counterPeriod++;
      }
    }

    int counterComma = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == ',')
      {
        counterComma++;
      }
    }

    int counterColon = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == ':')
      {
        counterColon++;
      }
    }

    int counterSemiColon = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == ';')
      {
        counterSemiColon++;
      }
    }

    int counterApostrophe = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '\'')
      {
        counterApostrophe++;
      }
    }

    int counterQuotation = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '\"')
      {
        counterQuotation++;
      }
    }

    int counterDash = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '�')
      {
        counterDash++;
      }
    }

    int counterExclamation = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '!')
      {
        counterExclamation++;
      }
    }

    int counterHyphen = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '-')
      {
        counterHyphen++;
      }
    }

    int counterOpenParentheses = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '(')
      {
        counterOpenParentheses++;
      }
    }

    int counterCloseParentheses = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == ')')
      {
        counterCloseParentheses++;
      }
    }

    int counterQuestion = 0;
    for (int i = 0; i < textStr.length(); i++)
    {
      if (textStr.charAt(i) == '?')
      {
        counterQuestion++;
      }
    }

    System.out.println("The ammount of periods in your text file is: "
                       + counterPeriod);

    System.out.println("The ammount of commas in your text file is: "
                       + counterComma);

    System.out.println("The ammount of colons in your text file is: "
                       + counterColon);

    System.out.println("The ammount of semicolons in your text file is: "
                       + counterSemiColon);

    System.out.println("The ammount of apostrophes in your text file is: "
                       + counterApostrophe);

    System.out.println("The ammount of quotation marks in your text file is: "
                       + counterQuotation);

    System.out.println("The ammount of dashes in your text file is: " 
                       + counterDash);

    System.out.println("The ammount of exclamation marks in your text file is: "
                       + counterExclamation);
    
    System.out.println("The ammount of hyphens in your text file is: "
                       + counterHyphen);

    System.out.println("The ammount of open parentheses' in your text file is: "
                       + counterOpenParentheses);

    System.out.println("The ammount of close parentheses' in your text file is: "
                       + counterCloseParentheses);

    System.out.println("The ammount of question marks in your text file is: "
                       + counterQuestion);

    fileScan.close();
  }

}
